const fs = require('node:fs');
const { Client, Collection, Intents } = require("discord.js");
const { joinVoiceChannel, createAudioPlayer, createAudioResource } = require('@discordjs/voice')
const { join } = require('node:path');
const { clientId, guildId, token } = require('./config.json');

const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_VOICE_STATES] });
exports.CLIENT = client;
const prefix = '·';
const player = createAudioPlayer();
const zik = ['l_internationale.mp3', 'fillon.mp3'];

client.commands = new Collection();
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
for (const file of commandFiles) {
    const command = require(`./commands/${file}`);
    // console.log(command.data.name)
    client.commands.set(command.data.name, command);
}

client.once("ready", () => {
    console.log("Ready !");
});

client.on('messageCreate', async message => {
    if (!message.guild) return;
    if (/(?=.*o)(?=.*l)(?=.*d)[old ]+$/gi.test(message.content)) message.delete();

	if (!message.content.startsWith(prefix) || message.author.bot) return;

	const args = message.content.slice(prefix.length).split(' ');
	const command = args.shift().toLowerCase();

    // TODO: Faire les commandes pour le point médian
});

client.on('interactionCreate', async interaction => {
    if (!interaction.isCommand()) return;

    const command = client.commands.get(interaction.commandName);

    if (!command) return;

    try {
        await command.execute(interaction);
    } catch (error) {
        console.error(error);
        await interaction.reply({ content: "Erreur", ephemeral: true });
    }
});

client.on('voiceStateUpdate', (old, now) => {
	if (now.channel && now.channel.name === 'Le Goulag' && now.channel.members.size <= 1 && old.channel?.id != now.channel.id) {
        const con = joinVoiceChannel({
            channelId: now.channel.id,
            guildId: now.channel.guild.id,
            adapterCreator: now.channel.guild.voiceAdapterCreator
          });
          
          con.subscribe(player);
          const resource = createAudioResource(join(__dirname, zik[Math.floor(Math.random() * zik.length)]));
          player.play(resource);
	}
});

client.login(token);
