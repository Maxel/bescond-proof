const { SlashCommandBuilder } = require('@discordjs/builders');
const { joinVoiceChannel, createAudioPlayer, createAudioResource } = require('@discordjs/voice')
const { join } = require('node:path');

const player = createAudioPlayer();

module.exports = {
  data: new SlashCommandBuilder()
    .setName('pedo')
    .setDescription('C\'était une adolescente.'),
  async execute(interaction) {
    const chan = interaction.member.voice.channel;
    if (chan) {
      const con = joinVoiceChannel({
        channelId: chan.id,
        guildId: chan.guild.id,
        adapterCreator: chan.guild.voiceAdapterCreator
      });
      
      con.subscribe(player);
      const resource = createAudioResource(join(__dirname, '../finkie_popol.flac'));
      player.play(resource);
    }
    interaction.reply({ content: 'Ah bon ?', ephemeral: true });
  }
};
