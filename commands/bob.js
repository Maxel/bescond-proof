const { SlashCommandBuilder } = require('@discordjs/builders');

const bob = (msg) => msg.toLowerCase().split('').map(x => Math.round(require('crypto').randomBytes(1)[0] / 256) ? x.toUpperCase() : x).join('');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('bob')
    .setDescription(bob('Ecrit en mocking bob'))
    .addStringOption(option => option.setName('bob').setDescription('Le texte à bobifier').setRequired(true)),
  async execute(interaction) {
    const msg = interaction.options.getString('bob');
    if (bob.length < 50) {
      await interaction.reply('https://mockingspongebob.org/' + encodeURI(msg));
    } else {
      await interaction.reply(bob(msg));
    }
  }
};
