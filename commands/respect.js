const { SlashCommandBuilder } = require('@discordjs/builders');
const { joinVoiceChannel, createAudioPlayer, createAudioResource } = require('@discordjs/voice')
const { join } = require('node:path');

const player = createAudioPlayer();

module.exports = {
  data: new SlashCommandBuilder()
    .setName('respect')
    .setDescription('Bonjour, Elise Lucet, France 2 !'),
  async execute(interaction) {
    const chan = interaction.member.voice.channel;
    if (chan) {
      const con = joinVoiceChannel({
        channelId: chan.id,
        guildId: chan.guild.id,
        adapterCreator: chan.guild.voiceAdapterCreator
      });
      
      con.subscribe(player);
      const resource = createAudioResource(join(__dirname, '../respect.mp4'));
      player.play(resource);
    }
    interaction.reply({ files: [{ attachment: 'respect.mp4' }] });
  }
};
