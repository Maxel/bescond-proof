const os = require('os');
const { SlashCommandBuilder } = require('@discordjs/builders');
const { join } = require('node:path');
const fs = require('fs');
const needle = require('needle');
const gm = require('gm').subClass({imageMagick: true});

const wat = join(__dirname, '../wat.png');
const source = join(os.tmpdir(), 'stadia_source.png');
const stadia_tmp = join(os.tmpdir(), 'stadia_tmp');
const stadia_final = join(os.tmpdir(), 'stadia_final.png');

const dim = ['53,55', '105,578', '703,415', '686,2'];

module.exports = {
  data: new SlashCommandBuilder()
    .setName('stadia')
    .setDescription('A utiliser à chaque fois que Bescond snap')
    .addStringOption(option => option.setName('url').setDescription('WTF ?! (PNG only)').setRequired(true)),
  async execute(interaction) {
    const url = interaction.options.getString('url');
    interaction.deferReply();

    needle.get(url).pipe(fs.createWriteStream(source)).on('done', function(err) {
	if (err) {
            console.log(err.message);
            interaction.editReply({ content: "Erreur de téléchargement", ephemeral: true });
            return;
        }
        const img = gm(source)
            .out('-alpha', 'Set')
            .out('-virtual-pixel', 'transparent')
            .out('-distort', 'Perspective')
            .out(`0,0,${dim[0]}
                0,%[height],${dim[1]}
                %[width],%[height],${dim[2]}
                %[width],0,${dim[3]}`)
            .out('-extent', '703x578')
            .out('-background', 'transparent')
            .write(stadia_tmp, function(err) {
                if (err) {
                    console.log(err.message);
                    interaction.editReply({ content: 'Erreur de conversion (lien en PNG only)', ephemeral: true });
                    return;
                }
                gm(wat).out(stadia_tmp)
                    .out('-compose', 'dst-over').out('-composite')
                    .write(stadia_final, function(err) {
                        if (err) {
                            console.log(err.message);
                            interaction.editReply({ content: 'Erreur de composition', ephemeral: true });
                            return;
                        }
                        interaction.editReply({ files: [{ attachment: stadia_final }] });
                    });
            });
    });

  }
};
