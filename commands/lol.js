const { SlashCommandBuilder } = require('@discordjs/builders');
const { joinVoiceChannel, createAudioPlayer, createAudioResource } = require('@discordjs/voice')
const { join } = require('node:path');

const player = createAudioPlayer();

module.exports = {
  data: new SlashCommandBuilder()
    .setName('lol')
    .setDescription('On va aller vite fait... jouer à LoL.'),
  async execute(interaction) {
    const chan = interaction.member.voice.channel;
    if (chan) {
      const con = joinVoiceChannel({
        channelId: chan.id,
        guildId: chan.guild.id,
        adapterCreator: chan.guild.voiceAdapterCreator
      });
      
      con.subscribe(player);
      const resource = createAudioResource(join(__dirname, '../jouer_a_lol.mp4'));
      player.play(resource);
    }
    interaction.reply({ files: [{ attachment: 'jouer_a_lol.mp4' }] });
  }
};
