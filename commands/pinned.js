const { SlashCommandBuilder } = require('@discordjs/builders');
// const { CLIENT } = require('../index');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('pinned')
    .setDescription('Répété et amplifié.')
    .addChannelOption(option => option.setName('channel').setDescription('Le lieu du désastre').setRequired(true)),
  async execute(interaction) {
    const chan = interaction.options.getChannel('channel') // CLIENT.channels.cache.get(interaction.channelId);
    const msgs = await chan.messages.fetchPinned();
    const msg = msgs.random();
    // message.channel.send(new Discord.MessageEmbed()
    //     .setAuthor(msg.author.username)
    //     .setDescription(msg.content)
    //     .setColor('#' + Math.random().toString(16).substr(2, 6))
    //     .setFooter(msg.createdAt.toLocaleDateString('fr-FR', {day: 'numeric', month: 'numeric', year: 'numeric'})));
    // message.channel.send('Contexte ? ' + msg.url);
    //console.log(msg)

    // chan.send(new Discord.MessageEmbed()
    //     .setAuthor({ name: msg.author.username })
    //     .setDescription(msg.content)
    //     .setFooter({ text: msg.createdAt.toLocaleDateString('fr-FR', {day: 'numeric', month: 'numeric', year: 'numeric'}) }));
    await interaction.reply("> " + msg.content + "\n*" + msg.author.username + "*, " + msg.createdAt.toLocaleDateString('fr-FR', {day: 'numeric', month: 'numeric', year: 'numeric'})
        + "\n" + msg.url);
    // await interaction.followUp('Contexte ? ' + msg.url);
  }
};
