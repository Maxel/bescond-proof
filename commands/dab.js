const { SlashCommandBuilder } = require('@discordjs/builders');

const bob = (msg) => msg.toLowerCase().split('').map(x => Math.round(require('crypto').randomBytes(1)[0] / 256) ? x.toUpperCase() : x).join('');

module.exports = {
  data: new SlashCommandBuilder()
    .setName('dab')
    .setDescription('Dab sur vos grands morts'),
  async execute(interaction) {
    await interaction.reply('https://i.imgur.com/Tj62ddc.gifv');
  }
};
